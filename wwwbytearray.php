<?php
define('BAD_CHARACTER', 1);
define('GOOD_CHARACTER', 0);

if(isset($_GET['action']) && isset($_GET['project'])) {
    $action = filterAction($_GET['action']);
    $projectName = $_GET['project'];
} else {
    giveBadRequestResponse();
}

if($action == 'ADD' || $action == 'DEL') {
    $unvalidatedCharacter = $_GET['char'];
    if(isValidCharacter($unvalidatedCharacter)) {
        $character = $unvalidatedCharacter;
    } else {
        giveBadRequestResponse();
    }
}

$projects = loadProjectsIntoMemory();
$isExistingProject = array_key_exists($projectName, $projects);
$isHash = false;

switch($action) {
    case 'ADD':
        if(!$isExistingProject) {
            $projects[$projectName] = getResetByteArray();
            $isExistingProject = 1;
        }
        $decimalVersionOfCharacter = hexdec($character);
        $projects[$projectName][$decimalVersionOfCharacter] = BAD_CHARACTER;
        break;
    case 'DEL':
        if($isExistingProject) {
            $decimalVersionOfCharacter = hexdec($character);
            $projects[$projectName][$decimalVersionOfCharacter] = GOOD_CHARACTER;
        }
        break;
    case 'RESET':
        if($isExistingProject) {
            unset($projects[$projectName]);
            $isExistingProject = 0;
        }
        break;
    case 'HASH':
        $isHash = true;
        break;
}

if($isExistingProject) {
    $currentProjectArray = $projects[$projectName];
} else {
    $currentProjectArray = getResetByteArray();
}

$stringToReturn = convertArrayToHexOutput($currentProjectArray);

if($isHash) {
    $stringToReturn = md5($stringToReturn);
}

print($stringToReturn);
updateProjects($projects);
die();

// End of logic in script.
function giveBadRequestResponse() {
    http_response_code(400);
    die();
}

function filterAction($unfilteredAction) {
    $uppercaseAction = strtoupper($unfilteredAction);
    switch($uppercaseAction) {
        case 'RESET':
            $filteredAction = 'RESET';
            break;
        case 'ADD':
            $filteredAction = 'ADD';
            break;
        case 'DEL':
            $filteredAction = 'DEL';
            break;
        case 'GET':
            $filteredAction = 'GET';
            break;
        case 'HASH':
            $filteredAction = 'HASH';
            break;
        default:
            giveBadRequestResponse();
    }
    return $filteredAction;
}

function getResetByteArray() {
    $genericData = array_fill(0, 256, '0');
    $genericData[0] = 1;
    return $genericData;
}

function convertArrayToHexOutput($array) {
    $hexOutput = '';
    foreach ($array as $decimalValue => $stateOfCharacter) {
        if(isGoodCharacter($stateOfCharacter)) {
            $newChar = sprintf("%02X", $decimalValue);
            $hexOutput .= $newChar;
        }
    }
    return $hexOutput;
}

function isGoodCharacter($characterState) {
    return ($characterState == GOOD_CHARACTER);
}

function isValidCharacter($char) {
    return ((hexdec($char) >= 0) && (hexdec($char) <= 255));
}

function updateProjects($projects) {
    file_put_contents('wwwbytearray.projects', serialize($projects));
}

function loadProjectsIntoMemory() {
    if(file_exists('wwwbytearray.projects')) {
        $projectData = unserialize(file_get_contents("wwwbytearray.projects"));
    } else {
        $projectData = array();
    }
    return $projectData;
}
